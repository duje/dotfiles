"Nvim initialization commands

se rtp^=~/.vim rtp+=~/.vim/after
let &pp = &rtp
so ~/.vim/.vimrc

se sdf=~/.local/share/nvim/shada/main.shada

if $TERM =~ '^\(alacritty\|screen\|st\|tmux\|xterm\)\(-.*\)\?$'
  se tgc
elsei $TERM =~ '^\(alacritty\|screen-256color\|st-256color\|tmux-256color\|xterm-256color\)\(-.*\)\?$'
  se tgc
el
  se notgc
en

"init.lua
lua require('init')
"{{{ https://github.com/jbyuki/instant.nvim
let g:instant_username = 'duje'
"}}}
"{{{ https://github.com/wbthomason/packer.nvim
lua require('plugins')

aug packer_user_config
  au!
  au BufWritePost plugins.lua so <afile> | PackerCompile
aug end
"}}}
