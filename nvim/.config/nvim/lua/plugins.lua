return require('packer').startup(function(use)
  -- {{{ https://github.com/ethanholz/nvim-lastplace
  use 'ethanholz/nvim-lastplace'
  -- }}}
  -- {{{ https://github.com/folke/persistence.nvim
  use({
    'folke/persistence.nvim',
    event = 'BufReadPre',
    module = 'persistence',
    config = function()
      require'persistence'.setup()
    end,
  })
  -- }}}
  -- {{{ https://github.com/ggandor/lightspeed.nvim
  use 'ggandor/lightspeed.nvim'
  -- }}}
  -- {{{ https://github.com/goolord/alpha-nvim
  use {
    'goolord/alpha-nvim',
    config = function()
      require'alpha'.setup(require'alpha.themes.dashboard'.config)
    end
  }
  -- }}}
  -- {{{ https://github.com/hrsh7th/nvim-cmp/
  use {
    'hrsh7th/nvim-cmp',
    config = function()
      require'cmp'.setup {
        snippet = {
          expand = function(args)
            require'luasnip'.lsp_expand(args.body)
          end
        },

        sources = {
          {
            name = 'luasnip'
          },
        },
      }
    end,
    requires = {
      'hrsh7th/cmp-buffer', -- https://github.com/hrsh7th/cmp-buffer
      'hrsh7th/cmp-calc', -- https://github.com/hrsh7th/cmp-calc
      'hrsh7th/cmp-cmdline', -- https://github.com/hrsh7th/cmp-cmdline
      'hrsh7th/cmp-nvim-lsp', -- https://github.com/hrsh7th/cmp-nvim-lsp
      'hrsh7th/cmp-nvim-lsp-signature-help', -- https://github.com/hrsh7th/cmp-nvim-lsp-signature-help
      'hrsh7th/cmp-path', -- https://github.com/hrsh7th/cmp-path
      'neovim/nvim-lspconfig' -- https://github.com/neovim/nvim-lspconfig
    }
  }
  -- }}}
  -- {{{ https://github.com/jbyuki/instant.nvim
  use 'jbyuki/instant.nvim'
  -- }}}
  -- {{{ https://github.com/kyazdani42/nvim-tree.lua
  use {
    'kyazdani42/nvim-tree.lua',
    requires = {
      'kyazdani42/nvim-web-devicons' -- https://github.com/kyazdani42/nvim-web-devicons
    }
  }
  -- }}}
  -- {{{ https://github.com/L3MON4D3/LuaSnip
  use {
    'L3MON4D3/LuaSnip',
    run = 'make install_jsregexp'
  }
  -- }}}
  -- {{{ https://github.com/leoluz/nvim-dap-go
  use 'leoluz/nvim-dap-go'
  -- }}}
  -- {{{ https://github.com/lukas-reineke/indent-blankline.nvim
  use 'lukas-reineke/indent-blankline.nvim'
  -- }}}
  -- {{{ https://github.com/mfussenegger/nvim-dap
  use 'mfussenegger/nvim-dap'
  -- }}}
  -- {{{ https://github.com/norcalli/nvim-colorizer.lua
  use 'norcalli/nvim-colorizer.lua'
  -- }}}
  -- {{{ https://github.com/nvim-telescope/telescope.nvim
  use {
    'nvim-telescope/telescope.nvim',
    tag = '0.1.0',
    requires = {
      'nvim-lua/plenary.nvim' -- https://github.com/nvim-lua/plenary.nvim
    },
  }
  use {
    'nvim-telescope/telescope-file-browser.nvim', -- https://github.com/nvim-telescope/telescope-file-browser.nvim
    requires = {
      'nvim-lua/plenary.nvim',
      'nvim-telescope/telescope.nvim'
    }
  }
  use {
    'nvim-telescope/telescope-frecency.nvim', -- https://github.com/nvim-telescope/telescope-frecency.nvim
    config = function()
      require'telescope'.load_extension('frecency')
    end
  }
  use {
    'nvim-telescope/telescope-media-files.nvim', -- https://github.com/nvim-telescope/telescope-media-files.nvim
    requires = {
      'nvim-lua/plenary.nvim',
      'nvim-lua/popup.nvim',
      'nvim-telescope/telescope.nvim'
    }
  }
  -- }}}
  -- {{{ https://github.com/nvim-treesitter/nvim-treesitter
  use {
    'nvim-treesitter/nvim-treesitter',
    run = ':TSUpdate'
  }
  -- }}}
  -- {{{ https://github.com/nvim-treesitter/nvim-treesitter-context
  use 'nvim-treesitter/nvim-treesitter-context'
  -- }}}
  -- {{{ https://github.com/saadparwaiz1/cmp_luasnip
  use 'saadparwaiz1/cmp_luasnip'
  -- }}}
  -- {{{ https://github.com/scalameta/nvim-metals/
  use {
    'scalameta/nvim-metals',
    requires = {
      'nvim-lua/plenary.nvim'
    }
  }
  -- }}}
  -- {{{ https://github.com/toppair/peek.nvim
  use({
    'toppair/peek.nvim',
    run = 'deno task --quiet build:fast'
  })
  -- }}}
  -- {{{ https://github.com/uga-rosa/cmp-dictionary
  use 'uga-rosa/cmp-dictionary'
  -- }}}
  -- {{{ https://github.com/wbthomason/packer.nvim
  use 'wbthomason/packer.nvim'
  -- }}}
  -- {{{ https://github.com/williamboman/mason.nvim
  use {
    'williamboman/mason.nvim',
    requires = {
      'williamboman/mason-lspconfig.nvim', -- https://github.com/williamboman/mason-lspconfig.nvim
      'neovim/nvim-lspconfig' -- https://github.com/neovim/nvim-lspconfig
    }
  }
  -- }}}
  -- {{{ https://github.com/windwp/nvim-autopairs
  use 'windwp/nvim-autopairs'
  -- }}}
end)
