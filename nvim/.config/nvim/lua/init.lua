-- enable true-color (24-bit) support
vim.api.nvim_set_option('termguicolors', true)
-- {{{ https://github.com/ethanholz/nvim-lastplace
require'nvim-lastplace'.setup {
  lastplace_ignore_buftype = {
    'help',
    'nofile',
    'quickfix'
  },
  lastplace_ignore_filetype = {
    'gitcommit',
    'gitrebase',
    'hgcommit',
    'svn'
  },
  lastplace_open_folds = true
}
-- }}}
-- {{{ https://github.com/goolord/alpha-nvim
local alpha = require('alpha')
local dashboard = require('alpha.themes.dashboard')
local version = vim.version()

dashboard.section.header.val = {'NVIM'}
dashboard.section.buttons.val = {
  dashboard.button('n', '⎀  New file', ':ene <BAR> startinsert <CR>'),
  dashboard.button('f', '⎁  Find file', ':Telescope find_files hidden=true path_display=smart<CR>'),
  dashboard.button('s', '"  Search string', ':Telescope live_grep<CR>'),
  dashboard.button('l', '⎗  Last session', ':lua require("persistence").load()<CR>'),
  dashboard.button('r', '⧖  Recently opened files', ':Telescope oldfiles<CR>'),
  dashboard.button('p', '…  Process view', ':term htop<CR>'),
  dashboard.button('u', '↻  Update plugins', ':PackerUpdate<CR>'),
  dashboard.button('i', '⚙  Init', ':e ~/.dotfiles/nvim/.config/nvim/lua/init.lua<CR>'),
  dashboard.button('q', '⏻  Quit', ':qa!<CR>'),
}

dashboard.section.footer.val = {
  " v" .. version.major .. "." .. version.minor .. "." .. version.patch
}

alpha.setup(dashboard.opts)

vim.cmd([[
    au FileType alpha setl nofen
]])
-- }}}
-- {{{ https://github.com/hrsh7th/nvim-cmp/
-- https://github.com/neovim/nvim-lspconfig/blob/master/doc/server_configurations.md
local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities = require'cmp_nvim_lsp'.default_capabilities(capabilities)

local lspconfig = require'lspconfig'

local servers = {
  'ansiblels',
  'dartls',
  'gopls',
  'graphql',
  'groovyls',
  'jdtls',
  'jsonnet_ls',
  'metals',
  'psalm',
  'puppet',
  'pyright',
  'rome',
  'rust_analyzer',
  'sorbet',
  'sourcekit',
  'sourcery',
  'svelte',
  'tailwindcss',
  'terraformls',
  'texlab',
  'yamlls'
}
for _, lsp in ipairs(servers) do
  lspconfig[lsp].setup {
    capabilities = capabilities,
  }
end

local icons = {
  Text = "⍺ ",
  Method = "M ",
  Function = "∗ ",
  Constructor = "⛏ ",
  Field = "⯐ ",
  Variable = "v ",
  Class = "Cls ",
  Interface = "⮺ ",
  Module = "Mod ",
  Property = "Prop ",
  Unit = "⯀ ",
  Value = "⯑ ",
  Enum = "Enum ",
  Keyword = "Keyw ",
  Snippet = "✄ ",
  Color = "Clr ",
  File = "⎁",
  Reference = "⭍ ",
  Folder = "⮷ ",
  EnumMember = "EnumMbr ",
  Constant = "∞ ",
  Struct = "Str ",
  Event = "⍾ ",
  Operator = "+ ",
  TypeParameter = "<T> "
}

local luasnip = require'luasnip'

local cmp = require'cmp'
cmp.setup {
  enabled = function()
    local context = require 'cmp.config.context'
    if vim.api.nvim_get_mode().mode == 'c' then
      return true
    else
      return not context.in_treesitter_capture'comment'
      and not context.in_syntax_group'comment'
    end
  end,
  enabled = function()
    buftype = vim.api.nvim_buf_get_option(0, "buftype")
    if buftype == "prompt" then return false end
    return true
  end,
  formatting = {
    format = function(entry, vim_item)
      vim_item.kind = string.format('%s %s', icons[vim_item.kind], vim_item.kind)
      vim_item.menu = ({
        buffer = "[Buffer]",
        nvim_lsp = "[LSP]",
        luasnip = "[LuaSnip]",
      })[entry.source.name]
      return vim_item
    end
  },
  mapping = cmp.mapping.preset.insert({
    ['<C-d>'] = cmp.mapping.scroll_docs(-4),
    ['<C-f>'] = cmp.mapping.scroll_docs(4),
    ['<C-Space>'] = cmp.mapping.complete(),
    ['<CR>'] = cmp.mapping.confirm {
      behavior = cmp.ConfirmBehavior.Replace,
      select = true,
    },
    ['<Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_next_item()
      elseif luasnip.expand_or_jumpable() then
        luasnip.expand_or_jump()
      else
        fallback()
      end
    end, {
    c = cmp.config.disable,
    'i',
    's' }),
    ['<S-Tab>'] = cmp.mapping(function(fallback)
      if cmp.visible() then
        cmp.select_prev_item()
      elseif luasnip.jumpable(-1) then
        luasnip.jump(-1)
      else
        fallback()
      end
    end, { 'i', 's' }),
  }),
  snippet = {
    expand = function(args)
      luasnip.lsp_expand(args.body)
    end,
  },
  sources = {
    {
      name = 'buffer',
      option = {
        get_bufnrs = function()
          local bufs = {}
          for _, win in ipairs(vim.api.nvim_list_wins()) do
            bufs[vim.api.nvim_win_get_buf(win)] = true
          end
          return vim.tbl_keys(bufs)
        end
      }
    },
    { name = 'calc' },
    { name = 'luasnip' },
    { name = 'nvim_lsp' },
    { name = 'nvim_lsp_signature_help' },
    { name = 'path' },
  },
  -- window = {
  --   completion = {
  --     border = border,
  --     winhighlight = 'Normal:CmpPmenu,FloatBorder:CmpPmenuBorder,CursorLine:PmenuSel,Search:None'
  --   },
  --   documentation = {
  --     border = border
  --   }
  -- },
}

cmp.setup.cmdline(':', {
  mapping = cmp.mapping.preset.cmdline(), -- important!
  sources = {
    { name = 'nvim_lua' },
    { name = 'cmdline' },
  },
})

cmp.setup.cmdline('/', {
  mapping = cmp.mapping.preset.cmdline(), -- important!
  sources = {
    { name = 'buffer' },
  },
})
-- }}}
-- {{{ https://github.com/kyazdani42/nvim-tree.lua
local function on_attach(bufnr)
  local api = require('nvim-tree.api')

  local function opts(desc)
    return { desc = 'nvim-tree: ' .. desc, buffer = bufnr, noremap = true, silent = true, nowait = true }
  end

  api.config.mappings.default_on_attach(bufnr)
  vim.keymap.set('n', '<C-]>', api.tree.change_root_to_node, opts('CD'))
  vim.keymap.set('n', '<C-e>', api.node.open.replace_tree_buffer, opts('Open: In Place'))
  vim.keymap.set('n', '<C-k>', api.node.show_info_popup, opts('Info'))
  vim.keymap.set('n', '<C-r>', api.fs.rename_sub, opts('Rename: Omit Filename'))
  vim.keymap.set('n', '<C-t>', api.node.open.tab, opts('Open: New Tab'))
  vim.keymap.set('n', '<C-v>', api.node.open.vertical, opts('Open: Vertical Split'))
  vim.keymap.set('n', '<C-x>', api.node.open.horizontal, opts('Open: Horizontal Split'))
  vim.keymap.set('n', '<BS>', api.node.navigate.parent_close, opts('Close Directory'))
  vim.keymap.set('n', '<CR>', api.node.open.edit, opts('Open'))
  vim.keymap.set('n', '<Tab>', api.node.open.preview, opts('Open Preview'))
  vim.keymap.set('n', '>', api.node.navigate.sibling.next, opts('Next Sibling'))
  vim.keymap.set('n', '<', api.node.navigate.sibling.prev, opts('Previous Sibling'))
  vim.keymap.set('n', '.', api.node.run.cmd, opts('Run Command'))
  vim.keymap.set('n', '-', api.tree.change_root_to_parent, opts('Up'))
  vim.keymap.set('n', 'a', api.fs.create, opts('Create'))
  vim.keymap.set('n', 'bmv', api.marks.bulk.move, opts('Move Bookmarked'))
  vim.keymap.set('n', 'B', api.tree.toggle_no_buffer_filter, opts('Toggle No Buffer'))
  vim.keymap.set('n', 'c', api.fs.copy.node, opts('Copy'))
  vim.keymap.set('n', 'C', api.tree.toggle_git_clean_filter, opts('Toggle Git Clean'))
  vim.keymap.set('n', '[c', api.node.navigate.git.prev, opts('Prev Git'))
  vim.keymap.set('n', ']c', api.node.navigate.git.next, opts('Next Git'))
  vim.keymap.set('n', 'd', api.fs.remove, opts('Delete'))
  vim.keymap.set('n', 'D', api.fs.trash, opts('Trash'))
  vim.keymap.set('n', 'E', api.tree.expand_all, opts('Expand All'))
  vim.keymap.set('n', 'e', api.fs.rename_basename, opts('Rename: Basename'))
  vim.keymap.set('n', ']e', api.node.navigate.diagnostics.next, opts('Next Diagnostic'))
  vim.keymap.set('n', '[e', api.node.navigate.diagnostics.prev, opts('Prev Diagnostic'))
  vim.keymap.set('n', 'F', api.live_filter.clear, opts('Clean Filter'))
  vim.keymap.set('n', 'f', api.live_filter.start, opts('Filter'))
  vim.keymap.set('n', 'g?', api.tree.toggle_help, opts('Help'))
  vim.keymap.set('n', 'gy', api.fs.copy.absolute_path, opts('Copy Absolute Path'))
  vim.keymap.set('n', 'H', api.tree.toggle_hidden_filter, opts('Toggle Dotfiles'))
  vim.keymap.set('n', 'I', api.tree.toggle_gitignore_filter, opts('Toggle Git Ignore'))
  vim.keymap.set('n', 'J', api.node.navigate.sibling.last, opts('Last Sibling'))
  vim.keymap.set('n', 'K', api.node.navigate.sibling.first, opts('First Sibling'))
  vim.keymap.set('n', 'm', api.marks.toggle, opts('Toggle Bookmark'))
  vim.keymap.set('n', 'o', api.node.open.edit, opts('Open'))
  vim.keymap.set('n', 'O', api.node.open.no_window_picker, opts('Open: No Window Picker'))
  vim.keymap.set('n', 'p', api.fs.paste, opts('Paste'))
  vim.keymap.set('n', 'P', api.node.navigate.parent, opts('Parent Directory'))
  vim.keymap.set('n', 'q', api.tree.close, opts('Close'))
  vim.keymap.set('n', 'r', api.fs.rename, opts('Rename'))
  vim.keymap.set('n', 'R', api.tree.reload, opts('Refresh'))
  vim.keymap.set('n', 's', api.node.run.system, opts('Run System'))
  vim.keymap.set('n', 'S', api.tree.search_node, opts('Search'))
  vim.keymap.set('n', 'U', api.tree.toggle_custom_filter, opts('Toggle Hidden'))
  vim.keymap.set('n', 'W', api.tree.collapse_all, opts('Collapse'))
  vim.keymap.set('n', 'x', api.fs.cut, opts('Cut'))
  vim.keymap.set('n', 'y', api.fs.copy.filename, opts('Copy Name'))
  vim.keymap.set('n', 'Y', api.fs.copy.relative_path, opts('Copy Relative Path'))
  vim.keymap.set('n', 'P', function()
    local node = api.tree.get_node_under_cursor()
    print(node.absolute_path)
  end, opts('Print Node Path'))

  vim.keymap.set('n', 'Z', api.node.run.system, opts('Run System'))
end

require'nvim-tree'.setup({
  on_attach = on_attach
})
-- }}}
-- {{{ https://github.com/leoluz/nvim-dap-go
require'dap-go'.setup()
-- }}}
-- {{{ https://github.com/L3MON4D3/LuaSnip
require'luasnip.loaders.from_snipmate'.lazy_load({paths = './snippets'}) -- :ec $MYVIMRC
-- }}}
-- {{{ https://github.com/lukas-reineke/indent-blankline.nvim
local highlight = {
  "IBL"
}

local hooks = require "ibl.hooks"
hooks.register(hooks.type.HIGHLIGHT_SETUP, function()
    vim.api.nvim_set_hl(0, "IBL", { fg = "#1c1c1c" })
end)

require('ibl').setup({
  exclude = {
    buftypes = {
      'nofile',
      'NvimTree',
      'terminal'
    },
    filetypes = {
      'alpha',
      'dashboard',
      'help',
      'lsp.log',
      'NvimTree',
      'packer'
    }
  },
  indent = {
    highlight = highlight
  }
})
-- }}}
-- {{{ https://github.com/nvim-telescope/telescope.nvim
require'telescope'.setup {
  extensions = {
    media_files = {
       filetypes = {
         'jpg',
         'jpeg',
         'mp4',
         'pdf',
         'png',
         'webm',
         'webp'
       },
       find_cmd = 'find'
    },
    frecency = {
      default_workspace = 'CWD',
      workspaces = {
        ['dot'] = '~/.dotfiles',
        ['x']    = '~/x',
        ['y']    = '~/y',
      }
    }
  }
}
require'telescope'.load_extension('file_browser')
require'telescope'.load_extension('media_files')
-- }}}
-- {{{ https://github.com/norcalli/nvim-colorizer.lua
require 'colorizer'.setup()
-- }}}
-- {{{ https://github.com/nvim-treesitter/nvim-treesitter
require'nvim-treesitter.configs'.setup {
  auto_install = true,
  ensure_installed = {
    'bash',
    'comment',
    'css',
    'dart',
    'dockerfile',
    'glsl',
    'go',
    'gomod',
    'gowork',
    'graphql',
    'hcl',
    'html',
    'http',
    'javascript',
    'json',
    'json5',
    'julia',
    'latex',
    'lua',
    'make',
    'markdown',
    'python',
    'regex',
    'ruby',
    'rust',
    'scala',
    'scss',
    'svelte',
    'vim',
    'yaml'
  },
  highlight = {
    additional_vim_regex_highlighting = false,
    enable = true,
  }
}
-- }}}
-- {{{ https://github.com/nvim-treesitter/nvim-treesitter-context
require'treesitter-context'.setup {
  enable = true,
  max_lines = 0,
  mode = 'cursor',
  patterns = {
    default = {
      'class',
      'function',
      'method',
      'for',
      'while',
      'if',
      'switch',
      'case'
    }
  },
  trim_scope = 'outer',
  zindex = 20
}
-- }}}
-- {{{ https://github.jkkcom/scalameta/nvim-metals
local nvim_metals_group = vim.api.nvim_create_augroup('nvim-metals', { clear = true })
vim.api.nvim_create_autocmd('FileType', {
  pattern = {
    'sbt',
    'scala'
  },
  callback = function()
    require'metals'.initialize_or_attach(metals_config)
  end,
  group = nvim_metals_group,
})
-- }}}
-- {{{ https://github.com/toppair/peek.nvim
require('peek').setup({
  auto_load = true,
  close_on_bdelete = true,
  syntax = true,
  theme = 'dark',
  update_on_change = true,
  app = 'browser',
  filetype = {
    'markdown'
  },
  throttle_at = 200000,
  throttle_time = 'auto'
})
vim.api.nvim_create_user_command('PeekOpen', require('peek').open, {})
vim.api.nvim_create_user_command('PeekClose', require('peek').close, {})
-- }}}
-- {{{ https://github.com/uga-rosa/cmp-dictionary
require'cmp_dictionary'.setup({
  dic = {
    spelllang = {
      de = "~/.dotfiles/aspell/de.dict",
      en = "~/.dotfiles/aspell/en.dict",
    },
  },
})
-- }}}
-- {{{ https://github.com/williamboman/mason-lspconfig.nvim
require("mason").setup()
require("mason-lspconfig").setup({
  ensure_installed = {
    'ansiblels',
    'bashls',
    'dockerls',
    'gopls',
    'jsonnet_ls',
    'pyright',
    'rust_analyzer',
    'tailwindcss',
    'terraformls',
    'yamlls'
  },
  ui = {
    icons = {
      package_installed = '✓',
      package_pending = '…',
      package_uninstalled = '✗',
    }
  }
})
-- }}}
-- {{{ https://github.com/windwp/nvim-autopairs
require('nvim-autopairs').setup({
  disable_filetype = {
    'TelescopePrompt',
    'vim'
  }
})
-- }}}
