export EDITOR=nvim

# disable .zsh_session folder
export SHELL_SESSIONS_DISABLE=1

# path to https://www.tug.org/mactex/BasicTeX.pdf
PATH=/Library/TeX/texbin:$PATH; export PATH
# PATH=/usr/local/texlive/2021basic/bin/universal-darwin:$PATH; export PATH

# https://crates.io/
export PATH="$HOME/.cargo/bin:$PATH"

source ~/.dotfiles/.env
