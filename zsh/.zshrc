# Z shell run commands

export EDITOR=nvim
export VISUAL=nvim

ANTIGEN_CACHE=false
source ~/antigen.zsh
# antigen bundle thewtex/tmux-mem-cpu-load # https://github.com/thewtex/tmux-mem-cpu-load
antigen bundle zsh-users/zsh-autosuggestions
antigen bundle zsh-users/zsh-syntax-highlighting
antigen bundle zsh-users/zsh-history-substring-search
antigen apply
ZSH_AUTOSUGGEST_STRATEGY=(history)
ZSH_AUTOSUGGEST_MANUAL_REBIND=1

# https://github.com/zsh-users/zsh-history-substring-search
bindkey -M vicmd 'j' history-substring-search-down
bindkey -M vicmd 'k' history-substring-search-up

bindkey '^a' beginning-of-line
bindkey '^e' end-of-line
bindkey '^l' clear-screen
bindkey '^R' history-incremental-pattern-search-backward
bindkey '^u' backward-kill-line

export LANG=en_US.UTF-8
export LC_ALL=en_US.UTF-8

setopt AUTO_CD # enable automatic change directory

HISTFILE=~/.history
HISTSIZE=200
SAVEHIST=200
unsetopt SHARE_HISTORY
setopt HIST_EXPIRE_DUPS_FIRST
setopt HIST_FIND_NO_DUPS
setopt HIST_IGNORE_ALL_DUPS
setopt HIST_IGNORE_SPACE
setopt HIST_REDUCE_BLANKS
setopt HIST_VERIFY
setopt INC_APPEND_HISTORY
setopt NO_HIST_BEEP

zstyle ':completion:*' verbose yes
zstyle ':completion:*::::' completer _expand _complete _ignored _approximate
zstyle ':completion:*:*:cp:*' file-sort modification reverse
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path "$HOME/zsh/.zcompcache"
zstyle ':completion:*' menu select
zstyle ':completion:*:*:-command-:*:*' group-order alias builtins functions commands
zstyle ':completion:*' file-list all
zstyle ':completion:*:matches' group 'yes'
zstyle ':completion:*:options' description 'yes'
zstyle ':completion:*:options' auto-description '%d'
# zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*:-tilde-:*' group-order 'named-directories' 'path-directories' 'users' 'expand'
zstyle ':completion:*' squeeze-slashes true

ZSH_AUTOSUGGEST_HIGHLIGHT_STYLE='fg=#343434'

zstyle ':completion:*:cd:*' ignore-parents parent pwd # disable prompt into current directory

# version control information
autoload -Uz vcs_info
precmd() { vcs_info }
zstyle ':vcs_info:git:*' formats '%F{#343434}%b%f' # format vcs_info_msg_0_ variable

# set prompt with git branch
setopt PROMPT_SUBST
PROMPT='%F{#00ffaf}%n@%m%f %F{#58a6ff}${PWD/#$HOME/~}%f ${vcs_info_msg_0_} '

# alias hotchpotch
alias ..='cd ..'
alias ...='cd ../../'
alias ....='cd ../../../'
alias -- -='cd -'
alias -g M='|more'
alias -g H='|head'
alias -g T='|tail'
alias asla="aspell -d $1 dump master | aspell -l $1 expand > ~/.dotfiles/aspell/$1.dict"
alias certbott="certbot certonly --manual --preferred-challenges dns --agree-tos -m '$1' -d '$2' '$3'"
alias chart='open http://www.calmar.ws/vim/256-xterm-24bit-rgb-color-chart.html'
alias cp='cp -iv'
alias cpd='cp -R'
alias critty='n ~/.dotfiles/alacritty/.config/alacritty/alacritty.yml'
alias dc=docker-compose
alias diff='diff -y --suppress-common-lines'
alias diffie='n ~/.dotfiles/vim/.vim/colors/diffie.vim'
alias digs='dig +short'
alias digns='dig ns +short'
alias digt='dig +trace'
alias dlog='docker-compose logs -f'
alias doc='cd ~/Documents'
alias dot='cd ~/.dotfiles'
alias down='cd ~/Downloads'
alias drm='docker rm $(docker ps -a -q)'
alias e='eza -alh --icons --git'
alias el='eza -lhTL=2 --icons --git'
alias fight="printf '(ง'̀-'́)ง' | pbcopy"
alias g=git
alias gc='n ~/.dotfiles/git/.gitconfig'
alias gcg='git config --global core.excludesFile ~/.dotfiles/git/.gitignore'
alias gcl=gcloud
alias ghl='gh auth login'
alias gi='n ~/.dotfiles/git/.gitignore'
alias gpgk='gpg --full-gen-key'
alias h='history -E'
alias hc=hcloud
alias he='h -E'
alias isgd='open https://is.gd/'
alias k=kubectl
alias lg=lazygit
alias ll='ls -aGhls --color=auto'
alias lx='latexmk -pdf -pvc' # add *.tex
alias mac="ifconfig en1 | awk '/ether/{print $1}'"
alias macn="openssl rand -hex 6 | sed 's/\(..\)/\1:/g; s/:$//'"
alias mkdir='mkdir -v'
alias mogs='mogrify -strip' # https://imagemagick.org/script/mogrify.php
alias grep='grep --color=auto'
alias n=nvim
alias nd='n -d'
alias ni='n ~/.dotfiles/nvim/.config/nvim/lua/init.lua'
alias np='n ~/.dotfiles/nvim/.config/nvim/lua/plugins.lua'
alias nv='n ~/.dotfiles/nvim/.config/nvim/init.vim'
alias neon='n ~/.dotfiles/vim/.vim/colors/neon.vim'
alias nip='ipconfig set en0 DHCP; ipconfig getpacket en0'
alias ns='netstat -i'
alias passc='pass -c'
alias passe='pass edit'
alias passg='pass generate -c'
alias passgn='pass generate -cn'
alias passm='pass mv'
alias qc='qemu-img create' # *.vmdk *.qcow2 -O qcow2
alias rc='n ~/.config/ranger/rc.conf'
alias rt='nettop -m route'
alias s=source
alias shrug="printf '¯\\_(ツ)_/¯' | pbcopy"
alias ssh='env TERM=xterm-256color ssh'
alias sudo='nocorrect sudo ' # https://www.zsh.org/mla/users/1999/msg00150.html
alias topo='top -o mem'
alias t=tmux
alias ta='tmux a -t' # add session name
alias tk='tmux kill-session -t' # add session name
alias tl='tmux list-sessions'
alias tf=terraform
alias tputc='tput colors'
alias ts='t new -s' # add session name
alias vd=vimdiff
alias vimrc='n ~/.dotfiles/vim/.vim/.vimrc'
alias vl='vim -V20 2>&1 | tee logfile'
alias vmap='n ~/.dotfiles/vim/.vim/plugin/mappings.vim'
alias vs=visudo
alias week='date +%V'
alias won='ifconfig en0 up'
alias woff='ifconfig en0 down'
alias wow="printf '(╯°□°)╯' | pbcopy"
alias xtar='tar -xvf' # convert .ova file
alias x='~/x'
alias y='~/y'
alias z=zathura
alias zp='n ~/.dotfiles/zsh/.zprofile'
alias zshrc='n ~/.dotfiles/zsh/.zshrc'
alias zshrcr='exec zsh -l'

# macOS
alias b=brew
alias be='brew edit' # add formula
alias bi='brew install' # add formula(e)
alias blaunchd='open https://www.launchd.info/'
alias browse='open -a /Applications/Firefox.app'
alias brs='brew reinstall --build-from-source' # add formula(e)
alias cng='scutil --get ComputerName'
alias cns='scutil --set ComputerName'
alias flush='dscacheutil -flushcache; killall -HUP mDNSResponder'
alias footi='n ~/.dotfiles/foot/.config/foot/foot.ini'
alias hng='scutil --get HostName'
alias hns='scutil --set HostName'
alias lhng='scutil --get LocalHostName'
alias lhns='scutil --set LocalHostName'
alias lsc='/System/Library/Frameworks/CoreServices.framework/Frameworks/LaunchServices.framework/Support/lsregister -kill -r -domain local -domain system -domain user && killall Finder'
alias m=mdls
alias pms='pmset sleepnow'
alias ports='networksetup -listallhardwareports'
alias si='mdutil -E' # add path
alias wd='networksetup -removepreferredwirelessnetwork en0'
alias wda='networksetup -removeallpreferredwirelessnetworks en0'
alias wj='networksetup -setairportnetwork en0'

# NixOS
alias afk="systemctl suspend && swaylock"
alias channel="sudo nix-channel --list | grep nixos"
alias channelup="sudo nix-channel --add https://channels.nixos.org/$1 nixos"
alias config='sudo n /etc/nixos/configuration.nix'
alias diskls='udisksctl status'
alias diske='eject' # /dev/
alias diskm='udisksctl mount -b' # /dev/
alias disku='udisksctl unmount -b' # /run/media/
alias fehf='feh -F --start-at'
alias fehs='feh --start-at --zoom fill'
alias garbage='sudo nix-collect-garbage -d'
alias gend='sudo nix-env --profile /nix/var/nix/profiles/system --delete-generations'
alias genls='sudo nix-env --profile /nix/var/nix/profiles/system --list-generations'
alias light='light -S'
alias nls='nix-channel --list'
alias nq='nix-env -q'
alias nup='nix-channel --update && nix-env -u'
alias qpdfm='qpdf --empty --pages *.pdf -- out.pdf'
alias reboot='sudo systemctl reboot'
alias shutdown='sudo shutdown -h now'
alias start='sudo systemctl start'
alias status='systemctl status'
alias stop='sudo systemctl stop'
alias switch='sudo nixos-rebuild switch'
alias wifi='nmcli device wifi list'
alias wific='nmcli device wifi connect' # password

# copy current command to clipboard
zle -N copyx; copyx() { echo -E $BUFFER | xsel -ib }; bindkey '^X' copyx

export MANPAGER='nvim +Man!'

# autoload functions
fpath=(~/myfuncs $fpath)
autoload -Uz fat ff gif gtd ipr mkcd pdff rmb rn rsyncc spider toucho

# transient prompt: https://www.zsh.org/mla/users/2019/msg00633.html
zle-line-init() {
emulate -L zsh

[[ $CONTEXT == start ]] || return 0

while true; do
zle .recursive-edit
local -i ret=$?
[[ $ret == 0 && $KEYS == $'\4' ]] || break
  [[ -o ignore_eof ]] || exit 0
done

local saved_prompt=$PROMPT
local saved_rprompt=$RPROMPT
PROMPT=''
RPROMPT=''
zle .reset-prompt
PROMPT=$saved_prompt
RPROMPT=$saved_rprompt

if (( ret )); then
  zle .send-break
else
  zle .accept-line
fi
return ret
}

zle -N zle-line-init

zmodload zsh/datetime

prompt_preexec() {
  prompt_prexec_realtime=${EPOCHREALTIME}
}

prompt_precmd() {
  if (( prompt_prexec_realtime )); then
    local -rF elapsed_realtime=$(( EPOCHREALTIME - prompt_prexec_realtime ))
    local -rF s=$(( elapsed_realtime%60 ))
    local -ri elapsed_s=${elapsed_realtime}
    local -ri m=$(( (elapsed_s/60)%60 ))
    local -ri h=$(( elapsed_s/3600 ))
    if (( h > 0 )); then
      printf -v prompt_elapsed_time '%ih%im' ${h} ${m}
    elif (( m > 0 )); then
      printf -v prompt_elapsed_time '%im%is' ${m} ${s}
    elif (( s >= 10 )); then
      printf -v prompt_elapsed_time '%.2fs' ${s} # 12.34s
    elif (( s >= 1 )); then
      printf -v prompt_elapsed_time '%.3fs' ${s} # 1.234s
    else
      printf -v prompt_elapsed_time '%ims' $(( s*1000 ))
    fi
    unset prompt_prexec_realtime
  else
    # clear previous result when hitting ENTER with no command to execute
    unset prompt_elapsed_time
  fi
}

setopt nopromptbang prompt{cr,percent,sp,subst}

autoload -Uz add-zsh-hook
add-zsh-hook preexec prompt_preexec
add-zsh-hook precmd prompt_precmd
RPROMPT='%F{#343434}${prompt_elapsed_time}%f'
