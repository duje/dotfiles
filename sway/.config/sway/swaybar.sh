#!/bin/sh
#
# Display swaybar.

while true; do
  battery_status=$(cat /sys/class/power_supply/BAT0/status)
  battery_capacity=$(cat /sys/class/power_supply/BAT0/capacity)
  date=$(date "+%a %H:%M %Y-%m-%d")

  echo -e "$battery_status $battery_capacity% $date"
  sleep 10  # update every ten second
done
