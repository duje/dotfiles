## dotfiles

This repository contains a list of dotfiles. Symlinks are managed with [stow][]. Run [make](Makefile).

[stow]: https://www.gnu.org/software/stow/

[Buy me a bit of time](https://www.buymeacoffee.com/duje) or

[![ko-fi](https://ko-fi.com/img/githubbutton_sm.svg)](https://ko-fi.com/T6T2JHALX)
