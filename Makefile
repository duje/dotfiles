STOW = stow -v -t $(HOME)
.PHONY: delete stow restow

delete:
	$(STOW) -D */

stow:
	$(STOW) */

restow:
	$(STOW) -R */
