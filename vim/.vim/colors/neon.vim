"{{{ :h syntax.txt
hi clear
if exists('syntax enable')
  sy reset
elsei exists('syntax on')
  sy reset
en

se bg=dark
let g:colors_name='neon'

let black = '#000000' "Black (0)
let red = '#ff7b72' "Red (1)
let green = '#3fb950' "Green (2)
let yellow = '#d29922' "Yellow (3)
let blue = '#58a6ff' "Blue (4)
let magenta = '#cc0099' "Magenta (5)
let cyan = '#39c5cf' "Cyan (6)
let white = '#ffffff' "White (7)
let black_bright = '#000000' "Bright Black (8)
let red_bright ='#ffa198' "Bright Red (9)
let green_bright = '#00ff99' "Bright Green (10)
let yellow_bright = '#e3b341' "Bright Yellow (11)
let blue_bright = '#79c0ff' "Bright Blue (12)
let magenta_bright = '#d2a8ff' "Bright Magenta (13)
let cyan_bright = '#56d4dd' "Bright Cyan (14)
let white_bright = '#ffffff' "Bright White (15)

exe 'hi Character guifg=' . blue
exe 'hi Constant guifg=' . blue
exe 'hi Directory guifg=' . blue
exe 'hi Function guifg=' . green_bright
exe 'hi Identifier guifg=' . blue_bright
exe 'hi Keyword guifg=' . blue
exe 'hi Name guibg=' . cyan
exe 'hi Operator guifg=' . white
exe 'hi SpecialKey guifg=' . white
exe 'hi Statement guifg=' . magenta
exe 'hi String guifg=' . red
exe 'hi Type guifg=' . cyan

hi ColorColumn ctermfg=NONE ctermbg=NONE guifg=NONE guibg=NONE
hi Comment guifg=#444444
hi CursorLine guibg=#121212
hi CursorLineNr guifg=#e4e4e4
hi DiffAdd guifg=#3fb950 guibg=NONE
hi DiffChange guifg=#e4e4e4 guibg=NONE
hi DiffDelete guifg=#ff7b72 guibg=NONE
hi DiffText guifg=white
hi EndOfBuffer guifg=black
hi ErrorMsg guifg=#ff7b72 guibg=NONE
hi Folded guifg=#5f5f5f guibg=#3a3a3a
hi FoldColumn guifg=#5f5f5f guibg=NONE
hi IncSearch guifg=#ffff00 guibg=NONE
hi LineNr guifg=#444444
hi MatchParen gui=bold guifg=#ffff00 guibg=NONE
hi ModeMsg guifg=#8a8a8a guibg=#3fb950
hi MsgArea guifg=white guibg=NONE
hi MoreMsg guifg=white guibg=NONE
hi NonText guifg=white guibg=NONE
hi Normal guifg=NONE guibg=NONE
hi NormalFloat guifg=white guibg=NONE
hi Pmenu guifg=white guibg=NONE
hi PmenuSbar guifg=NONE guibg=#121212
hi PmenuSel guifg=#ffffff guibg=#005fd7
hi PmenuThumb guifg=NONE guibg=#1c1c1c
hi Question guifg=#ff87af guibg=NONE
hi QuickFixLine guifg=white guibg=#005fd7
hi SignColumn guifg=#5f5f5f guibg=NONE
hi SpellBad guifg=NONE guibg=NONE gui=undercurl
hi StatusLine guibg=#1c1c1c gui=NONE
hi StatusLineNC guifg=NONE guibg=#626262
hi Substitute guifg=#ffa198 guibg=NONE
hi Visual guifg=NONE guibg=#949494
hi WarningMsg guifg=#ff7b72 guibg=NONE
hi WildMenu guifg=white guibg=#005fd7
hi VertSplit guifg=#1c1c1c gui=NONE

hi! link DiffChange DiffDelete
hi! link Search IncSearch
hi! link NormalNC NormalFloat
hi! link LineNrAbove LineNr
hi! link LineNrBelow LineNr
hi! link SpellCap SpellBad
hi! link SpellLocal SpellBad
hi! link SpellRare SpellBad
hi! link Title StatusLine
hi! link TabLine StatusLineNC
hi! link TabLineFill StatusLineNC
hi! link TabLineSel StatusLineNC
hi! link VisualNOS Visual
hi! link WhiteSpace NonText

hi GitStatus guifg=#ef391a

"plugin/statusline.vim
hi StatusLineNormal guibg=#005fd7
hi StatusLineCommand guibg=#005fd7
hi StatusLineTerminal guibg=#005f00

exe 'hi StatusLineInsert guibg=' . green
exe 'hi StatusLineReplace guibg=' . red
exe 'hi StatusLineVisual guibg=' . green
exe 'hi StatusLineVisualL guibg=' . yellow
exe 'hi StatusLineVisualB guibg=' . yellow
exe 'hi StatusLineCommand guibg=' . magenta
exe 'hi StatusLineSelect guibg=' . yellow
exe 'hi StatusLineSelectL guibg=' . yellow
exe 'hi StatusLineSelectB guibg=' . yellow
exe 'hi StatusLineSelect guibg=' . yellow
