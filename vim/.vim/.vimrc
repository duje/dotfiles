"VIM - run commands

cal plug#begin() "https://github.com/junegunn/vim-plug
"{{{ https://github.com/matze/vim-move
Plug 'matze/vim-move'
"}}}
"{{{ https://github.com/ryanoasis/vim-devicons
Plug 'ryanoasis/vim-devicons'
"}}}
"{{{ https://github.com/tridactyl/tridactyl
Plug 'tridactyl/vim-tridactyl'
"}}}
"{{{ https://github.com/turbio/bracey.vim
Plug 'turbio/bracey.vim', { 'do': 'npm install --prefix server' }
"}}}
"{{{ https://github.com/wfxr/minimap.vim
Plug 'wfxr/minimap.vim', { 'do': ':!cargo install --locked code-minimap' } "}}}
"}}}
cal plug#end()

colo neon
filet plugin indent on

"{{{ :h bracey
let g:bracey_auto_start_browser = 2
let g:bracey_eval_on_save = 2
let g:bracey_refresh_on_save = 2
let g:bracey_server_log = '/tmp/bracey_server_logfile'
let g:bracey_server_path = 'http://127.0.0.1'
"}}}

"{{{ :h minimap
let g:minimap_auto_start = 1
let g:minimap_auto_start_win_enter = 1
let g:minimap_block_filetypes = ['alpha', 'dashboard', 'nerdtree', 'netrw', 'txt']
let g:minimap_block_buftypes = ['alpha', 'dashboard', 'nofile', 'nowrite', 'quickfix', 'terminal', 'prompt']
let g:minimap_close_filetypes = ['alpha', 'dashboard', 'netrw', 'vim-plug']
let g:minimap_width = 10
"}}}

"{{{ :h move.txt
let g:move_key_modifier = 'C'
let g:move_key_modifier_visualmode = 'S'
"}}}
