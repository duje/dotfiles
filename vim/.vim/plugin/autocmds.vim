"{{{ :h 'cul'
aug CursorLineOnlyInActiveWindow
  au!
  au VimEnter,WinEnter,BufWinEnter * set cul
  au WinLeave * set nocul
aug END
"}}}

"{{{ :h 'cc', draw column when longest line in file exceeds lengthLimit
so ~/.vim/autoload/colorcolumn.vim
aug ShowColorColumn
  au!
  au BufRead,TextChanged,TextChangedI * cal ShowColorColumnIfLineTooLong(80)
aug END
"}}}

"{{{ dart-flutter.snippets
au BufRead,BufNewFile,BufEnter *.dart UltiSnipsAddFiletypes dart-flutter
"}}}

"au BufWritePost *.tex exe "!latexmk -pdf -pvc %" | redr!
