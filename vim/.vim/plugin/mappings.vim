"{{{ :h map-commands

let mapleader = 'm'

ino <silent> Left> <Esc><Up>
ino <silent> <Right> <Esc><Down>
ino <silent> <Up> <Esc><Up>
ino <silent> <Down> <Esc><Down>

"leave Insert-mode on 'ii'
"im ii <Esc>

"call (off) column
nn <Leader>ch :se cc=<CR>
nn <Leader>cc :se cc=81<CR>

"highlight stop
nn <Leader>hs :noh<CR> :echom ''<CR>

"beautify json
nn <Leader>jb :se ft=json<CR>:%!python -m json.tool<CR>gg=G<CR>

"netrw
nn <Leader>le :Lexplore<CR>

"NvimTree
nn <Leader>ln :NvimTreeToggle<CR>
nn <Leader>r :NvimTreeRefresh<CR>
nn <Leader>ff :NvimTreeFindFile<CR>
nn <Leader>rs :NvimTreeResize<CR>

"search replace: case insensitive, sensitive, confirm, and insert
nn <Leader>ir :%s///g
nn <Leader>sr :%s///gI
nn <Leader>qr :%s///gcI

nn <Leader>ts :Telescope<Space>
nn <Leader>fb :Telescope file_browser<CR>
nn <Leader>fr :Telescope frecency<CR>
nn <Leader>lg :Telescope live_grep<CR>
nn <Leader>ws :Telescope lsp_workspace_symbols<CR>
nn <Leader>of :Telescope oldfiles<CR>

"term
nn <Leader>te :term<CR>
nn <Leader>op :vsplit term://top
nn <Leader>z :botright 20sp term://zsh<CR>i

"quickfix
nn <Leader>q :copen<CR>
nn <Leader>qf :cfdo %!sed -E 's///g

"buffer movement
nn <S-h> <C-w>h
nn <S-j> <C-w>j
nn <S-k> <C-w>k
nn <S-l> <C-w>l
nn <S-f> gt
nn <S-d> gT
nn <Leader>tn :tabnew<CR>
nn <Leader>tc :tabclose<CR>

"delete with <BS>
nn <Tab> i<Tab>
nn <BS> i<BS><Esc>`^i

"jump x lines
nn <Leader>+[count]j<CR>

"back and forward
nn <Leader>mb <C-o>
nn <Leader>mf <S-C-o>

"29.1 go-to tag
nn <Leader>l <C-]>
nn <Leader>t <C-t>

"folds
"za, zM, zR

"{{{ https://github.com/L3MON4D3/LuaSnip
im <silent><expr> <Tab> luasnip#expand_or_jumpable() ? '<Plug>luasnip-expand-or-jump' : '<Tab>'
ino <silent> <S-Tab> <cmd>lua require'luasnip'.jump(-1)<CR>
snor <silent> <Tab> <cmd>lua require'luasnip'.jump(1)<CR>
snor <silent> <S-Tab> <cmd>lua require'luasnip'.jump(-1)<CR>
im <silent><expr> <C-e> luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-e>'
smap <silent><expr> <C-e> luasnip#choice_active() ? '<Plug>luasnip-next-choice' : '<C-e>'
"}}}
"{{{ https://github.com/leoluz/nvim-dap-go
nn <silent> <Leader>dgo <Cmd>lua require'dap-go'.debug_test()<CR>
"}}}
"{{{ https://github.com/mfussenegger/nvim-dap
nn <silent> <Leader>d <Cmd>lua require'dap'.continue()<CR>
nn <silent> <Leader>do <Cmd>lua require'dap'.step_over()<CR>
nn <silent> <Leader>di <Cmd>lua require'dap'.step_into()<CR>
nn <silent> <Leader>dt <Cmd>lua require'dap'.step_out()<CR>
nn <silent> <Leader>b <Cmd>lua require'dap'.toggle_breakpoint()<CR>
nn <silent> <Leader>B <Cmd>lua require'dap'.set_breakpoint(vim.fn.input('Breakpoint condition: '))<CR>
nn <silent> <Leader>dp <Cmd>lua require'dap'.set_breakpoint(nil, nil, vim.fn.input('Log point message: '))<CR>
nn <silent> <Leader>dr <Cmd>lua require'dap'.repl.open()<CR>
nn <silent> <Leader>dl <Cmd>lua require'dap'.run_last()<CR>
"}}}
"{{{ https://github.jkkcom/scalameta/nvim-metals
nn <silent> <Leader>sm <Cmd>lua require'metals'.hover_worksheet()<CR>
"}}}
"{{{ https://github.com/uga-rosa/cmp-dictionary
nn <Leader>de CmpDictionaryUpdate de<CR>
nn <Leader>en CmpDictionaryUpdate en<CR>
"}}}
"}}}
