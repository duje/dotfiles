"{{{ :h netrw
let g:netrw_banner = 0
let g:netrw_hide = 1
let g:netrw_keepdir = 0
let g:netrw_liststyle = 4
let g:netrw_rmdir_cmd='rm -r'
let g:netrw_winsize = 15
"}}}
