"{{{ :h 'stl'
func GitBranch()
  retu system("git rev-parse --abbrev-ref HEAD 2>/dev/null | tr -d '\n'")
endf

func GitStatusLine()
  let b:branch = GitBranch()
  let b:git_branch =' ' . b:branch
  retu strlen(b:branch) > 0?' '.b:git_branch.' ':''
  exe 'hi b:git_branch guifg=#ef391a guibg=NONE gui=NONE'
endf

let g:status_line_mode = {
      \ 'n': {'mode': 'normal', 'highlight': 'StatusLineNormal'},
      \ 'i': {'mode': 'insert', 'highlight': 'StatusLineInsert'},
      \ 'R': {'mode': 'replace', 'highlight': 'StatusLineReplace'},
      \ 'v': {'mode': 'visual', 'highlight': 'StatusLineVisual'},
      \ 'V': {'mode': 'v·line', 'highlight': 'StatusLineVisualL'},
      \ "\<C-v>": {'mode': 'v-block', 'highlight': 'StatusLineVisualB'},
      \ 'c': {'mode': 'command', 'highlight': 'StatusLineCommand'},
      \ 's': {'mode': 'select', 'highlight': 'StatusLineSelect'},
      \ 'S': {'mode': 's·line', 'highlight': 'StatusLineSelectL'},
      \ "\<C-s>": {'mode': 's·block', 'highlight': 'StatusLineSelectB'},
      \ 't': {'mode': 'terminal', 'highlight': 'StatusLineTerminal'},
      \ }

func StatusLineMode()
  let current_mode = mode()
  if has_key(g:status_line_mode, current_mode)
    retu g:status_line_mode[current_mode]['mode']
  en
  retu mode
endf

func! StatusLineModeHighlight()
  let current_mode = mode()
  if has_key(g:status_line_mode, current_mode)
    retu "%#" . g:status_line_mode[current_mode]['highlight'] . "#"
  en
  retu "%#StatusLine#"
endf
"}}}
