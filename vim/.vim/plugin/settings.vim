"{{{ :h set-option
se noswf "no swapfile
se stl= "left side
se stl+=%{%StatusLineModeHighlight()%}
se stl+=\ %{%StatusLineMode()%}
se stl+=\ %#StatusLine#
se stl+=\ %n "buffer number
se stl+=\ %{GitStatusLine()}
se stl+=\ %<%f%m%r%h%w "file path, modified, readonly, helpfile, preview
se stl+=\ %{&ff} "fileformat
se stl+=\ %{&fenc?&fenc:&enc} "encoding
se stl+=\ %{WebDevIconsGetFileTypeSymbol()}
se stl+=%= "right side
se stl+=%*\ :\%3.l/%L\ \ ℅\ \%-3.c\ %-3.p%% "column, line number, total lines, percentage of document
"se stl+=%*\ :\%l/%L\ \ ℅:\%c\ %-3.p%% "column, line number, total lines, percentage of document
set clipboard+=unnamedplus
se rnu "relative line number
se si "smart indent on new line
se sm "match inserted bracket
se so=8 "lines above and below cursor
se sts=2 "spaces <Tab> counts while editing
se sw=2 "value of 'shiftwidth'
se ts=2 "spaces <Tab> counts for

setl acd "working directory change on file opening
setl ai "copy indent current to new line
setl bs=indent,eol,start "backspace over ai, lbr and start
setl cot=menu,menuone,noselect "one match popup menu force select
"setl culopt=both "cursorline
setl et "convert tab to space
setl fcs+=fold:\  "vertical separator
setl fdm=marker "fmr
setl fde=nvim_treesitter#foldexpr()
setl hid "buffer becomes hidden
setl ic "ignore case insensitivity in search pattern
setl is "show matches while searching
setl list lcs=tab:!·,trail:· "trail display
setl lsp=0 "pixel between chars
setl ls=3 "status line only last window
setl mh "hidden mouse while typing
setl mouse=a "mouse support all modes
setl nobk "no backup before overwriting file
setl nosmd "hide modes
setl nu "print line number
setl nuw=3 "line number columns
setl pa=$PWD/** "match up to 30 directories by default
setl ph=10 "popup menu items
setl ve=onemore "allow cursor move past eol
setl wig+=**/.git/**,**/__pycache__/**,**/venv/**,**/node_modules/**
setl wim=list:longest:full,full "complete match
setl wmnu "command-line completion
setl wop=pum  "popup completion matches

if &diff
	hi CursorLine guifg=white guibg=#121212
	se dip-=internal
	se dip=iwhite
en
"}}}
