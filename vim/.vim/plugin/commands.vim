com -nargs=1 SwitchLang cal s:switch_lang('<args>')

":Telescope grep_string, <C-q> for qf 
com! -nargs=? Tgrep lua require 'telescope.builtin'.grep_string({ search = vim.fn.input("Grep for…  ")})
