"{{{ https://github.com/uga-rosa/cmp-dictionary/wiki/Examples-of-usage
func! s:switch_lang(lang) abort
  exe 'se spelllang=' . a:lang
  CmpDictionaryUpdate
endf
"}}}
