func! ShowColorColumnIfLineTooLong(lengthLimit)
	if &ft =~ 'alpha'
		retu
	en

  let maxLineLength = max(map(getline(1,'$'), 'len(v:val)'))

  if maxLineLength > a:lengthLimit
    hi ColorColumn guifg=NONE guibg=#121212
    "draw column at first letter that exceeds the limit
    exe "setl cc=" . (a:lengthLimit + 1)
  el
    setl cc=""
  en
endf
